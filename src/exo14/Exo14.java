package exo14;


import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exo14 {

	public static void main(String[] args) {

		List<String> strings = List.of(
				"one", "two", "three", "four", "five", "six", "seven",
				"eight", "nine", "ten", "eleven", "twelve");
		
		//Question1:
		    Stream<String> elements = 
		    strings.stream();
		    System.out.println("\nles �l�ments de ce stream:\n");
		    elements.forEach(System.out::println);
		
		//Question2:
		    Stream<String> majuscule = 
			strings.stream().map(word -> word.toUpperCase());
			System.out.println("\nles �l�ments de ce stream mis en majuscule\n");
			majuscule.forEach(System.out::println);
			
		//Question3:
			Stream<Character> premierelettre = 
			strings.stream().map(word -> word.toUpperCase().charAt(0)).distinct();
			System.out.println("\nles premi�res lettres des �l�ments de ce stream, mises en majuscules, sans doublon\n");
			premierelettre.forEach(System.out::println);
			
	   //Question4:	
			Stream<Integer> longueur = 
			strings.stream().map(word -> word.length()).distinct();
			System.out.println("\nles longueurs des �l�ments de ce stream, sans doublon\n");
			longueur.forEach(System.out::println);
			 		
	  //Question5:		
			long nmbrelement = 
	        strings.stream().count();
			System.out.println("\nle nombre d��l�ments de ce stream \n"+ nmbrelement);
			
	 //Question6:	
			long nmbrelementpair = 
			strings.stream().filter(word -> word.length()% 2 == 0).count();
			System.out.println("\nle nombre d��l�ments de ce stream qui ont une longueur paire\n "+ nmbrelementpair);
			
	//Question7:		
            long  longueurlong = 
			strings.stream().map(word -> word.length()).max(Integer::compare).get();
			System.out.println( "\nla longueur de la cha�ne la plus longue\n "+longueurlong);
		
	//Question8:		
			List<String> majusculeimpaire =
			strings.stream().map(word -> word.toUpperCase()).filter(word -> word.length()% 2 != 0).collect(Collectors.toList());
			System.out.println( "\nla liste des cha�nes de longueur impaire, mises en majuscules \n ");
			majusculeimpaire.forEach(System.out::println);
			
    //Question9:
			String concatenation = 
			strings.stream() .filter(word -> word.length()<= 5).sorted().collect(Collectors.joining(" , ", "{", "}"));
			System.out.println("\nla concat�nation des cha�nes\n" + concatenation);
		
	//Question10:
			Map<Integer, List<String>> maplongueur = 
			strings.stream() .collect(Collectors.groupingBy(String::length));
			System.out.println("\nles longueurs des cha�nes et leurs listes  associee\n ");
			maplongueur.forEach((key, value) -> System.out.println(key + " = " + value));
			
   //Question11:
			Map<Character, String> maplettre = 
			strings.stream().collect( Collectors.groupingBy( word -> word.charAt(0),Collectors.joining(", ") )) ;
		    System.out.println("\nles cl�s sont les premi�res lettres de ces cha�nes et les valeurs la concat�nation de ces cha�ne");
			maplettre.forEach((key, value) -> System.out.println(key + " est associ�e � la cha�ne " + value));
	}
}
